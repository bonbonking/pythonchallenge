"""
with the hint in the page source code, change
http://www.pythonchallenge.com/pc/def/channel.html
to
http://www.pythonchallenge.com/pc/def/channel.zip

readme.txt indicates:

welcome to my zipped list.

hint1: start from 90052
hint2: answer is inside the zip
"""
import zipfile

pattern = "channel/%s.txt"
fn = ""
comments = []
zipf = zipfile.ZipFile("channel.zip")
hasNext = True

while hasNext:
  if not fn:
    fp = pattern % 90052

  try:
    with open(fp, "r") as f:
      str = "".join([line.rstrip() for line in f])
      fn = [s for s in str.split() if s.isdigit()][0]
      fp = pattern % fn
      comments.append(zipf.getinfo(fn + ".txt").comment)
  except:
    hasNext = False
    print "The end."

print "".join(comments)


"""
till this step, i got this:

Collect the comments.

Since it says "answer is inside the zip", is there such a thing as comment
in zip file? Search for zip module

yep.

http://www.pythonchallenge.com/pc/def/hockey.html

look closer

http://www.pythonchallenge.com/pc/def/oxygen.html
"""
