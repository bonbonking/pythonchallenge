from string import ascii_lowercase

str = "g fmnc wms bgblr rpylqjyrc gr zw fylb. rfyrq ufyr amknsrcpq ypc dmp. bmgle gr gl zw fylb gq glcddgagclr ylb rfyr'q ufw rfgq rcvr gq qm jmle. sqgle qrpgle.kyicrpylq() gq pcamkkclbcb. lmu ynnjw ml rfc spj."
alphabet_list = list(ascii_lowercase)
off_alphabet_list = []

# apprently I'm too lazy to type abcdefg...z myself

for idx, val in enumerate(alphabet_list):
    print idx, val
    if ((len(alphabet_list) - 2) > idx):
      print "put " + alphabet_list[idx + 2]
      off_alphabet_list.append(alphabet_list[idx + 2])
    else:
      off_alphabet_list.append(alphabet_list[idx + 2 - len(alphabet_list)])

print off_alphabet_list

mapping_str = ""

for c in str:
  if (c in alphabet_list):
    idx = alphabet_list.index(c)
    mapped_c = off_alphabet_list[idx]
    mapping_str += mapped_c
  else:
    mapping_str += c

print mapping_str

"""result
i hope you didnt translate it by hand. thats what computers are for.
doing it in by hand is inefficient and that's why this text is so long.
using string.maketrans() is recommended. now apply on the url.
"""

"""solution
http://www.pythonchallenge.com/pcc/def/ocr.html

Damn this is so much smarter :)

>>> string.ascii_lowercase[2:]
>>> 'cdefghijklmnopqrstuvwxyz'
>>> string.ascii_lowercase[:2]
>>> 'ab'
"""
