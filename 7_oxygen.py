"""
obviously the information is hidden inside the gray pixels

print all the gray pixels

"""

from PIL import Image

im = Image.open("oxygen.png")
w, h = im.size
msg = []

# use the INTERVAL thing in range!!!
# incredibly useful

for x in range(0, w, 7):
  r, g, b, a = im.getpixel((x, h/2))
  if (r == g) and (g == b):
    msg.append(chr(r))

print "".join(msg)


"""
result

smart guy, you made it. the next level is
[105, 110, 116, 101, 103, 114, 105, 116, 121]

=> integrity

http://www.pythonchallenge.com/pc/def/integrity.html
"""
