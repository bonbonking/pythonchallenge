"""
http://www.pythonchallenge.com/pc/def/linkedlist.php
http://www.pythonchallenge.com/pc/def/linkedlist.php?nothing=12345
http://www.pythonchallenge.com/pc/def/linkedlist.php?nothing=44827
http://www.pythonchallenge.com/pc/def/linkedlist.php?nothing=45439
http://www.pythonchallenge.com/pc/def/linkedlist.php?nothing=94485
....

it's going on and on and on
so the challenge is basically to use python to get to the end of the thing!
"""
import requests

root = "http://www.pythonchallenge.com/pc/def/linkedlist.php?nothing=%d"
url = root % 12345
has_next = True

while has_next:
  r = requests.get(url)
  if r.status_code == 200:
    print r.text
    str = r.text
    digit = [int(s) for s in str.split() if s.isdigit()]
    if digit:
      next = digit[0]
      url = root % next
    else:
      next = next / 2
      url = root % next

    print url
  else:
    has_next = False


"""
result: peak.html

http://www.pythonchallenge.com/pcc/def/peak.html
"""
