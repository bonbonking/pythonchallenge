# rare characters: those appeared less frequent
import collections

with open('2_ocr.txt', "r") as f:
  str = "".join([line.rstrip() for line in f])
  ocr = collections.OrderedDict()
  for c in str:
    ocr[c] = ocr.get(c, 0) + 1

  # average occurence
  print "Total characters: %d Unique characters: %d" % (len(str), len(ocr))
  avgOc = len(str) // len(ocr)

  # print characters which appears less often
  print ''.join([c for c in str if ocr[c] < avgOc])

"""result
equality
"""

"""solution:
http://www.pythonchallenge.com/pcc/def/equality.html
"""
