"""
One small letter, surrounded by EXACTLY
three big bodyguards on each of its sides.

solution: create the regular expression to find this type of string
"""

import re

pattern = r'(?<![A-Z])[A-Z]{3}[a-z][A-Z]{3}(?![A-Z])'
rgx = re.compile(pattern)

with open('3_equality.txt', "r") as f:
  str = "".join([line.rstrip() for line in f])
  result = rgx.findall(str)
  print result

"""
linkedlist
['IQNlQSL', 'OEKiVEY', 'ZADnMCZ', 'ZUTkLYN', 'CNDeHSB', 'OIXdKBF', 'XJVlGZV', 'ZAGiLQZ', 'CJAsACF', 'KWGtIDC']
"""

"""answer
http://www.pythonchallenge.com/pc/def/linkedlist.html
"""
